#include<iostream>
#include<string>

using namespace std;

class vozac
{
	private:
		string _ime;
		string _prezime;
		int _godiste;
		string _tim;
	public:
		vozac();
		vozac(string ime, string prezime, int godiste,string tim);
		vozac(string ime, string prezime, string tim, int godiste) :
_ime(ime), _prezime(prezime), _tim(tim), _godiste(godiste) {}

		string ime() const;
		string prezime() const;
		int godiste() const;
		string tim();
		void promjeniTim(string noviTim);
};

string vozac::ime() const
{
	return _ime;
}

string vozac::prezime() const
{
	return _prezime;
}

int vozac::godiste() const
{
	return _godiste;
}

string vozac::tim()
{
	return _tim;
}

void vozac::promjeniTim(string noviTim)
{
	_tim = noviTim;
}

vozac::vozac()
{
	_ime = "nepoznato";
	_prezime = "nepoznato";
	_godiste = 0;
	_tim = "nepoznato";
}

vozac::vozac(string ime, string prezime, int godiste,string tim)
{
	_ime = ime;
	_prezime = prezime;
	_godiste = godiste;
	_tim = tim;
}

int main()
{
	vozac michael = vozac("michael","schumacher",1969,"ferrari");
	vozac nepoznat = vozac();
	michael.promjeniTim("renault");

	// napraviti ispis
	return 0;
}

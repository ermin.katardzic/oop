#ifndef _GUMA_H
#define _GUMA_H
#include <iostream>
#include <string>

class guma {
private:
    int _r;
    std::string _tip;
public:

    guma() : _r(15), _tip("intermediate") {std::cout << "Tip gume: " << _tip << std::endl;}
    guma(int r, const std::string tip) : _r(r), _tip(tip) {}
    const std::string & tip() const {return _tip;}
    void tip(const std::string & tip) {_tip = tip;}
};
#endif
#ifndef _FORMULA_H
#define _FORMULA_H
#include <iostream>
#include <string>
#include "guma.h"

class formula {
private:
  guma _prednje;
  guma _zadnje;
  std::string _ekipa;
//	std::string _vozac;

public:
  formula() {std::cout<<"Default konstruktor za formulu"<<std::endl;};

	formula(const guma &prednje, const guma &zadnje, const std::string &ekipa): _ekipa(ekipa), _prednje(prednje){
		_zadnje = zadnje;
		std::cout<<"Drugi konstruktor za formulu"<<std::endl;
  }

  const std::string& ekipa() const {return _ekipa;}
  void promjenaGuma(const std::string& tip);
  void ispisTipaGuma() const;
  void ekipa(const std::string& ekipa) {_ekipa = ekipa;}
};
#endif


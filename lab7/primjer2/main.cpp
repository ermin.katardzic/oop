#include<iostream>
#include<string>
#include"formula.h"
using namespace std;

int main() {

	guma prednja;
	guma zadnja(17, "intermediate");

	formula f1;
  formula f2(prednja, zadnja, "Ferrari");

  f1.ekipa("BMW");
  f2.promjenaGuma("soft");

  cout << "Prvi u cilju je: " << f2.ekipa() << endl;

  cout << "Pobjednik koristi ";
  f2.ispisTipaGuma();

  cout << "Drugo mjesto: " << f1.ekipa() << endl;

return 0;}

#include <iostream>
#include <string>
#include "formula.h"

void formula::promjenaGuma(const std::string& tip) {
    _prednje.tip(tip);
    _zadnje.tip(tip);
}

void formula::ispisTipaGuma() const {
    std::cout << "tip guma: " << _prednje.tip() << std::endl;
}

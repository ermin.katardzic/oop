#include <iostream>

using namespace std;

int main() {
	int x = 5;
	int &y = x;
	int z = 3;

	int &a;
	int &b = 6;
	const int &c = x;
	c = 13;

	cout << y << endl;
	cout << x << endl;

	//Referenca se mora inicializirati pri deklaraciji i ne moze se vise mijenjati
	y = z;

	cout << y << endl;
	cout << x << endl;
}

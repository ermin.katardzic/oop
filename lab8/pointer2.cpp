#include<iostream>
#include<iterator>
#include<algorithm>
#include<vector>

using namespace std;

int main() {
	int br_el;

	cout << "Unesite broj elemenata niza: ";
	cin >> br_el;

	int *niz = new int[br_el];

	for (int i = 0; i < br_el; i++) {
		niz[i] = i;
		//*(niz+i) = i;
	}

	for (int const *p{niz}; p < niz + br_el; p++) {
		std::cout << *p << std::endl;
	}

	return 0;
}


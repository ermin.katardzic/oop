#include <iostream>

int main()
{
	int *p_int{nullptr}, i{10}, j;
	std::string s("abcd");
	std::string *p_str{&s};
	
	std::cout << *p_str << std::endl;

	p_int = &i;
	*p_int = 100;
	p_int = &j;
	*p_int = i;

	std::cout << p_str->size() << std::endl;

	std::cout << j << std::endl;
	std::cout << i << std::endl;

	return 0;
}

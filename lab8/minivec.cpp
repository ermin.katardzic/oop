#include <iostream>
#include <string>
#include <initializer_list>
#include <algorithm>

using namespace std;

class minivec {
  public:
    minivec() { cout << "minivec konstruktor"  << endl;}
    minivec(initializer_list<int> lista) : podaci_{new int[n_]}, n_{lista.size()} {copy(begin(lista), end(lista), podaci_);}

    int size() const {return n_;}
    ~minivec() { cout << "minivec destruktor"  << endl; delete [] podaci_; }
  private:
    size_t n_{0};
    int* podaci_{nullptr};
};


int main()
{
  minivec f1{1, 2, 3, 4, 5};
	minivec ftest;

  minivec* p_f2 = new minivec[5];
  delete []p_f2;
  p_f2 = nullptr;
  delete []p_f2;

  cout << "Br. el: " << f1.size() <<endl;
  
  return 0;
}

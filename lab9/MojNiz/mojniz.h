#ifndef _MOJ_NIZ
#define _MOJ_NIZ
#include <algorithm>
#include <initializer_list>
#include <iostream>
class MojNiz {
  public:
  MojNiz(std::initializer_list<int> a) : p_{new int[n_]}, n_{a.size()} {
    std::copy(begin(a), end(a), p_);
  }

  MojNiz(const MojNiz& drugi){
  }

  MojNiz(MojNiz&& drugi){
  }

  MojNiz& operator=(const MojNiz& drugi);
  MojNiz& operator=(MojNiz&& drugi);
  ~MojNiz() { delete[] p_; }
  size_t size() const { return n_; }
  int& at(size_t i) { return p_[i]; }
  MojNiz& operator+=(const MojNiz&);
  int& operator[](size_t i) { return p_[i]; }
  const int& operator[](size_t i) const { return p_[i]; }

  private:
  size_t n_{0};
  int* p_{nullptr};
};

MojNiz operator+(MojNiz, const MojNiz&);
std::ostream& operator<<(std::ostream&, const MojNiz&);

#endif

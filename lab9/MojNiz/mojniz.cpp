#include "mojniz.h"

MojNiz& MojNiz::operator=(const MojNiz& drugi) {
  return *this;
}

MojNiz& MojNiz::operator=(MojNiz&& drugi) {
  return *this;
}

MojNiz& MojNiz::operator+=(const MojNiz& drugi) {
  if (n_ != drugi.n_) throw std::string("nekompatibilne dimenzije");
  for (size_t i = 0; i < n_; ++i) p_[i] += drugi.p_[i];
  return *this;
}

MojNiz operator+(MojNiz a, const MojNiz& b) {
  a += b;
  return a;
}

std::ostream& operator<<(std::ostream& out, const MojNiz& a) {
  for (size_t i = 0; i < a.size(); ++i) out << a[i] << " ";
  return out;
}

#include "mojniz.h"

int vratiNiz(bool t) {
	if (t) {
		return MojNiz{9, 9, 9};
	} else {
		return MojNiz{6, 6, 6}
	}
}

int main() {
  MojNiz a{2, 3, 4};
  MojNiz b = a + MojNiz{2, 1, 1} + a;
  a += b;
	MojNiz c = a;
	c = MojNiz{6, 7, 8};
	MojNiz d = vratiNiz;

  std::cout << b << std::endl;
  std::cout << a << std::endl;
  return 0;
}

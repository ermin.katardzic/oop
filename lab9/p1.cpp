#include <iostream>
#include <string>

using namespace std;


class Foo {
  public:
    Foo() {cout << "Default constructor\n";}
    Foo(const Foo& x) : m_{x.m_} {cout << "Copy constructor\n";}
    // Foo(Foo&& x) = delete;
    Foo(Foo&& x) : m_{x.m_} {cout << "Move constructor\n";}
    explicit Foo(int m) : m_{m} {cout << "Conversion from int = " << m << endl;}
    ~Foo() {cout <<"Destructor\n";}
    operator int() const {return m_;}
  private:
    int m_{0};
};

int foobar(const Foo &a) {
  cout << a << endl;
  return a;
}

int main(int argc, char *argv[])
{
  Foo a;
  Foo b = a;
  Foo c = move(a);
  Foo d = Foo{5};

  foobar(Foo{5});
}


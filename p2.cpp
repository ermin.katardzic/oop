#include <iostream>

using namespace std;

int utrostruci1(int br) {
	br *= 3;
	return br;
}

int utrostruci2(int& br) {
	br *= 3;
}

/*
int utrostruci3(const int& br) {
	br *=3;
}
*/

int pomnozi(int& br, const int& mn) {
	br *= mn;
}

int main() {
	int broj;
	cin >> broj;

	/*
	int rezultat;
	rezultat = utrostruci1(broj);
	cout << rezultat << " " << broj << endl;
	*/

	/*
	utrostruci2(broj);
	cout << broj << endl;
	*/

	/*
	int mnozilac;
	cin >> mnozilac;

	pomnozi(broj, mnozilac);
	cout << broj << endl;
	*/
}

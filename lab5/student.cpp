#include "student.h"

using namespace std;

void ispisStudenata(const list<student>& lista){

	cout <<"Ispis liste studenata: " << endl;
	for(listaStudConstIter k = lista.begin(); k != lista.end(); k++){
		cout << k->ime << '\t' << k->prezime << '\t' << k->indeks << '\t' << k->prosjek << '\t' << endl;	
	}

}

bool poIndeksu(const student& a, const student& b){
	return a.indeks < b.indeks;
}

void sortiranjeStudenata(list<student>& lista){

	lista.sort(poIndeksu);
}

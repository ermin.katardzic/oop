#include<iostream>
#include<list>
#include<string>
#include"student.h"


using namespace std;


int main(){

	string ime;
	string prezime;
	string indeks;
	double prosjek;
	
	list<student> lista;

	cout << "Unesite ime, prezime, indeks i prosjek studenta: " << endl;
	
	while(cin >> ime >> prezime >> indeks >> prosjek)
	{
		student temp;
		temp.ime = ime;
		temp.prezime = prezime;
		temp.indeks = indeks;
		temp.prosjek = prosjek;

		lista.push_back(temp);
		
	}

	ispisStudenata(lista);
	sortiranjeStudenata(lista);
	ispisStudenata(lista);

return 0;}

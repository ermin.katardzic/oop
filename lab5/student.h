#ifndef _STUDENT_H
#define _STUDENT_H
#include <list>
#include <iostream>

struct student{

	std::string ime;
	std::string prezime;
	std::string indeks;
	double prosjek;
};

typedef std::list<student> listaStud;
typedef std::list<student>::iterator listaStudIter;
typedef std::list<student>::const_iterator listaStudConstIter;


void ispisStudenata(const std::list<student>&);
void sortiranjeStudenata(std::list<student>&);


#endif

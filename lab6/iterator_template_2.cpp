#include <iostream>
#include <list>
#include <vector>
using namespace std;

template <typename T, typename U>
void moj_copy(T poc, T kraj, U dest) {
  while (poc != kraj) {
    *dest = *poc;
    ++dest;
    ++poc;
  }
}

int saberi(int a, int b) {
  return a + b;
}


template <typename T, typename U>
T saberi (T a, U b) {
  return a + b;
}

int main() {
  vector<int> v1{3, 4, 5};
  vector<int> v2(5);
  list<int> l1;
  
	moj_copy(begin(v1), end(v1), begin(v2));

	cout << "Vektor: " << endl;
	for (const int& el : v2) {
		cout << el << " ";
	}
	cout << endl;

  moj_copy(begin(v1), end(v1), front_inserter(l1));
  moj_copy(begin(l1), end(l1), back_inserter(v2));
  
	cout << "Vektor: " << endl;
	for (const int& el : v2) {
		cout << el << " ";
	}

	cout << endl;
	cout << "Lista: " << endl;
	for (const int& el : l1) {
		cout << el << " ";
	}

  cout << endl;
  cout << sizeof(saberi(3l, 5)) << endl;
  return 0;
}

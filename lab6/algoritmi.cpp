//Program koji sabira sve cifre u stringu koji se unese sa tastature
#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <algorithm>
#include <cctype>

using namespace std;

bool nije_cifra(const char& c)
{
    return !isdigit(c);
}

int u_broj(const char& b)
{
    return b - '0';
}

int vrati_sumu(const string& ulaz)
{
    vector<int> brojevi;
    string cifre;
    //remove_copy_if(begin(ulaz), end(ulaz), back_inserter(cifre), nije_cifra);
    remove_copy_if(begin(ulaz), end(ulaz), back_inserter(cifre), [](char c){return !isdigit(c); });
    transform(begin(cifre), end(cifre), back_inserter(brojevi), u_broj);
    return accumulate(begin(brojevi), end(brojevi),0);
}

int main()
{
    cout<<"Unesite string koji sadrzi cifre!"<<endl;
    string ulaz;
    getline(cin,ulaz);
    cout<< "Rezultat(suma svih cifara u stringu) je: "<< vrati_sumu(ulaz)<<endl;
    return 0;
}

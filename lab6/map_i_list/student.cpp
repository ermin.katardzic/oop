#include <iostream>
#include <stdexcept>
#include "student.h"

using namespace std;

istream& unosStudent(istream& unos, list<student>& studenti)
{
    if (unos)
    {
        studenti.clear();
        student temp;
        cout<<"Unos studenata: ime, prezime i prosjek! EOF za prekid!"<<endl;
        while (unos >> temp.ime >> temp.prezime >> temp.prosjek)
        {
            
            if(temp.prosjek > 10.0 || temp.prosjek < 6.0)
                throw domain_error("Prosjek nije validan!\n");
            studenti.push_back(temp);
        }
    }

    unos.clear();
    return unos;
}

ostream& ispisStudent(ostream& izlaz, const list<student>& studenti)
{
    for(list<student>::const_reverse_iterator i=studenti.rbegin();i!=studenti.rend();++i)
        izlaz<<(*i).ime<<' '<<i->prezime<<' '<<i->prosjek<<endl;

    return izlaz;
}

bool poProsjeku(const student &a, const student &b)
{
    return a.prosjek > b.prosjek;
}


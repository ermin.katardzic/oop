#include<iostream>
#include<map>
#include<list>
#include<string>
#include"student.h"

using namespace std;

int main() {
    typedef map<string, list<student> > mapa;
    mapa mapaStud;
    string predmet;

    while (1) {
        cout << "Unijeti naziv predmeta. EOF za prekid." << endl;
        if(!(cin >> predmet))
            break;
        auto it = mapaStud.find(predmet);
        if(it != end(mapaStud))
        {
          cout << "Unos zanemaren!" << endl;
          continue;
        }

        list<student> listaStud;
				try {
					unosStudent(cin, listaStud);
				}
				catch (domain_error e) {
					cout << e.what() << endl;
				}
        mapaStud[predmet]=listaStud;
    }

    for (auto i = begin(mapaStud); i != end(mapaStud); ++i)
    {
        cout << "Predmet" << i->first << endl;
        ispisStudent(cout,i->second);
        list<student> temp = i->second;
        //i->second.sort(poProsjeku);
        temp.sort(poProsjeku);
        ispisStudent(cout,temp);
    }
    
    return 0;
}

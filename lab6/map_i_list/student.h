#ifndef _STUDENT_H
#define _STUDENT_H

#include <iostream>
#include <string>
#include <list>

struct student
{
    std::string ime;
    std::string prezime;
    double prosjek;
};


bool poProsjeku(const student &, const student &);
std::istream& unosStudent(std::istream&, std::list<student>&);
std::ostream& ispisStudent(std::ostream&, const std::list<student>&);
#endif


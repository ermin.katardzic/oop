#include <vector>
#include <iostream>
#include <algorithm>
#include <list>

using namespace std;


template <typename T>
void ispisi(const T& kont) {
		cout << "Ispis liste: " << endl;
    for(const auto& el : kont)
    {
        cout << el << endl;
    }
}

int main()
{
    //list<int> vec{1,3,5,5,2,4,7,6,5};
    vector<int> vec{1,3,5,5,2,4,7,6,5};

    auto iter = find(begin(vec), end(vec), 4);
    cout  <<  "Broj je: " << *iter << endl;

    // auto it=begin(vec);
    // while(it != end(vec))
    // {
    //     if(*it == 5)
    //         it = vec.erase(it);
    //     else
    //         ++it;
    // }
    
		// remove ne brise elemente, samo ih pusha na kraj kontejnera
		ispisi(vec);

    auto it = remove(begin(vec), end(vec), 5);
    cout << "Velicina vectora: " << vec.size() << endl;
		ispisi(vec);

    vec.erase(it, end(vec));
    cout << "Velicina vectora: " << vec.size()<<endl;

    ispisi(vec);

    cout << "Broj je: " << *iter << endl;

		auto ins = inserter(vec, iter);
		*ins = 99;

		ispisi(vec);
    cout << "Broj je: " << *iter << endl;

    return 0;
}

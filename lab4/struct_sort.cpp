#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

struct osoba {
	string ime;
	int godiste;
};

void ispisi(const vector<osoba>);

int main() {
	osoba o1, o2, o3;
	vector<osoba> v;

	o1.ime = "John";
	o1.godiste = 1985;

	o2.ime = "Dave";
	o2.godiste = 1990;

	o3.ime = "Jim";
	o3.godiste = 1982;

	v.push_back(o1);
	v.push_back(o2);
	v.push_back(o3);

	//sort(v.begin(), v.end());
	
	// Nije potrebno naglasiti povratnu vrijednost kada ima samo return
	auto sortiranje = [](const osoba &os1, const osoba &os2) -> bool {return os1.godiste < os2.godiste;};

	sort(v.begin(), v.end(), sortiranje);

	ispisi(v);
}

void ispisi(const vector<osoba> v) {
	for (int i = 0; i < v.size(); i++) {
		cout << v.at(i).ime << " " << v.at(i).godiste << endl;
	}

	/*
	for (int i = 0; i < v.size(); i++) {
		osoba o = v.at(i);
		cout << o.ime << " " << o.godiste << endl;
	}
	*/
}

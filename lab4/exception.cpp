#include <iostream>

using namespace std;

struct iznimka {
	int broj_iznimke;
	string opis_iznimke;
};

void test1() {
	throw string("Iznimka string");
}

void test2() {
	iznimka iz;
	iz.broj_iznimke = 500;
	iz.opis_iznimke = "Nesto se pokvarilo";

	throw iz;
}

void test3() {
	throw 51;
}

int main() {
	try{
		test1();
		//test2();
		//test3();
	}
	catch(string e) {
		cout << e << endl;
	}
	catch(iznimka e) {
		cout << e.broj_iznimke << " " << e.opis_iznimke << endl;
	}
	catch(...) {
		cout << "Uhvacena iznimka ciji tip nije posebno tretiran" << endl;
	} 
}

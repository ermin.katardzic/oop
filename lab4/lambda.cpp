#include<iostream>

using namespace std;

int main(){
  int x = 5;
  float y = 10.5;
  auto f = [x, &y](int a) mutable{
    cout << "U lambdi a je: " << a << endl;
    cout << "U lambdi x je: " << x++ << endl;
    cout << "U lambdi y je: " << y++ << endl;
  };

  f(4);
	cout << "Van lambde x je: " << x << endl;
	cout << "Van lambde y je: " << y << endl;
  x += 2;
  f(8);
	cout << "Van lambde x je: " << x << endl;
	cout << "Van lambde y je: " << y << endl;
  x += 2;
  x += 2;
  y += 2.2;
  f(6);
	cout << "Van lambde x je: " << x << endl;
	cout << "Van lambde y je: " << y << endl;
  x += 2;
  x += 2;
  return 0;
}

